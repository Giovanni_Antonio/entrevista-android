package giovanni.app.androidpeliculasseries.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import giovanni.app.androidpeliculasseries.R
import giovanni.app.androidpeliculasseries.model.Movie
import giovanni.app.tools.BuildConfig
import kotlinx.android.synthetic.main.row_movi_and_series.view.*


class MovieAdapter(private val movies: List<Movie>) : RecyclerView.Adapter<MovieAdapter.MovieViewHolder>(){

    class MovieViewHolder(view : View) : RecyclerView.ViewHolder(view),View.OnClickListener{
        private val IMAGE_BASE = BuildConfig.SMALL_IMAGE_URL
        fun bindMovie(movie : Movie){
            itemView.txtMovie.text = movie.title
            itemView.textFecha.text = movie.release
            Picasso.get().load(IMAGE_BASE + movie.poster).into(itemView.image_View_movie)
        }
        init {
            itemView.setOnClickListener(this)
        }
        override fun onClick(p0: View?) {
            val pos = adapterPosition
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.row_movi_and_series, parent, false)
        )
    }

    override fun getItemCount(): Int = movies.size

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bindMovie(movies.get(position))
        val movileResponse = movies!![position]


    }




}