package giovanni.app.androidpeliculasseries.ui.Activitys
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import giovanni.app.androidpeliculasseries.R
import giovanni.app.androidpeliculasseries.model.Movie
import giovanni.app.tools.BuildConfig
import kotlinx.android.synthetic.main.activity_movie_details.*

class MovieDetailsActivity : AppCompatActivity() {
    var movie: Movie? = null
    private val IMAGE_BASE = BuildConfig.SMALL_IMAGE_URL

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)
        datos()
    }


    private fun datos() {
        val intent = intent
        if (intent.extras != null) {
            movie = intent.getSerializableExtra("data") as Movie?
            edt_nombre_movie.setText(movie?.original)
            edt_descripcion_movie.setText(movie?.overview)
            edt_nombre_popular.setText(movie?.popularity)
            edt_nombre_vote.setText(movie?.vote)
            edt_nombre_fecha.setText(movie?.release)
            Picasso.get().load(IMAGE_BASE + movie?.poster).into(image_View_movile)
        }
    }

}