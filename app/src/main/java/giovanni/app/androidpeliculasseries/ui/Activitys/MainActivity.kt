package giovanni.app.androidpeliculasseries.ui.Activitys

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import giovanni.app.androidpeliculasseries.R
import giovanni.app.androidpeliculasseries.model.Movie
import giovanni.app.androidpeliculasseries.services.MovieApiService
import giovanni.app.androidpeliculasseries.services.interfaces.MovieApiInterface
import giovanni.app.androidpeliculasseries.services.tools.MovieResponse
import giovanni.app.androidpeliculasseries.ui.adapters.MovieAdapter
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    var moviesAdapter: MovieAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        shimmer_recycler_view.layoutManager = LinearLayoutManager(this)
        getMovieData { movies : List<Movie> ->
            shimmer_recycler_view.adapter = MovieAdapter(movies)
        }
        shimmer_recycler_view.showShimmerAdapter()

    }

    private fun getMovieData(callback: (List<Movie>) -> Unit){
        val apiService = MovieApiService.getInstance().create(MovieApiInterface::class.java)
        apiService.getMovieList().enqueue(object : Callback<MovieResponse> {
            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {

            }

            override fun onResponse(call: Call<MovieResponse>, response: Response<MovieResponse>) {
                shimmer_recycler_view.hideShimmerAdapter()
                return callback(response.body()!!.movies)
            }

        })
    }
    fun ClickedUser(movieResponse: MovieResponse?) {
        startActivity(
            Intent(this, MovieDetailsActivity::class.java).putExtra(
                "data",
                movieResponse
            )
        )
    }
}