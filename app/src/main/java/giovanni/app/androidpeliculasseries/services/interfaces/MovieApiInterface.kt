package giovanni.app.androidpeliculasseries.services.interfaces

import giovanni.app.androidpeliculasseries.services.tools.MovieResponse
import giovanni.app.tools.BuildConfig
import retrofit2.Call
import retrofit2.http.GET

interface MovieApiInterface {

    @GET(BuildConfig.TMBD_API_KEY)
    fun getMovieList(): Call<MovieResponse>
}