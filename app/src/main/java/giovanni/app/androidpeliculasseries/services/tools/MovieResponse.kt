package giovanni.app.androidpeliculasseries.services.tools

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import giovanni.app.androidpeliculasseries.model.Movie
import kotlinx.android.parcel.Parcelize


@Parcelize
data class MovieResponse(
    @SerializedName("results")
    val movies : List<Movie>

) : Parcelable {
    constructor() : this(mutableListOf())
}