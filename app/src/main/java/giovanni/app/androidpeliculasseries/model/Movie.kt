package giovanni.app.androidpeliculasseries.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Movie(
    @SerializedName("id")
    val id : String ?,

    @SerializedName("title")
    val title : String?,

    @SerializedName("poster_path")
    val poster : String?,

    @SerializedName("release_date")
    val release : String?,

    @SerializedName("overview")
    val overview : String?,

    @SerializedName("vote_average")
    val vote : String?,

    @SerializedName("popularity")
    val popularity : String?,

    @SerializedName("video")
    val video : String?,

    @SerializedName("original_title")
    val original : String?,

) : Parcelable{
    constructor() : this("", "", "", "", "", "", "", "", "")
}