package giovanni.app.androidpeliculasseries.database.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import giovanni.app.androidpeliculasseries.database.Entry.MovieEntry
import giovanni.app.androidpeliculasseries.database.dao.MovieDao


@Database(entities = [MovieEntry::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao

}

