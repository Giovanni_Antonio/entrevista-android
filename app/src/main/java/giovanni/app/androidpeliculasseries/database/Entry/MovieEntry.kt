package giovanni.app.androidpeliculasseries.database.Entry

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey


@Entity(tableName = "Movietetable")
class MovieEntry {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private var id = 0

    @ColumnInfo(name = "movieid")
    private var movieid = 0

    @ColumnInfo(name = "title")
    private var title: String? = null

    @ColumnInfo(name = "userrating")
    private var userrating: Double? = null

    @ColumnInfo(name = "posterpath")
    private var posterpath: String? = null

    @ColumnInfo(name = "overview")
    private var overview: String? = null

    @Ignore
    fun FavoriteEntry(
        movieid: Int,
        title: String?,
        userrating: Double?,
        posterpath: String?,
        overview: String?
    ) {
        this.movieid = movieid
        this.title = title
        this.userrating = userrating
        this.posterpath = posterpath
        this.overview = overview
    }

    fun FavoriteEntry(
        id: Int,
        movieid: Int,
        title: String?,
        userrating: Double?,
        posterpath: String?,
        overview: String?
    ) {
        this.id = id
        this.movieid = movieid
        this.title = title
        this.userrating = userrating
        this.posterpath = posterpath
        this.overview = overview
    }

    fun getId(): Int {
        return id
    }

    fun setId(id: Int) {
        this.id = id
    }

    fun getMovieid(): Int {
        return movieid
    }

    fun setMovieid(movieid: Int) {
        this.movieid = movieid
    }

    fun getTitle(): String? {
        return title
    }

    fun setTitle(title: String?) {
        this.title = title
    }

    fun getUserrating(): Double? {
        return userrating
    }

    fun setUserrating(userrating: Double?) {
        this.userrating = userrating
    }

    fun setPosterpath(posterpath: String?) {
        this.posterpath = posterpath
    }

    fun getPosterpath(): String? {
        return posterpath
    }

    fun setOverview(image: String?) {
        overview = overview
    }

    fun getOverview(): String? {
        return overview
    }
}