package giovanni.app.androidpeliculasseries.database.dao

import androidx.room.OnConflictStrategy.IGNORE
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.*
import giovanni.app.androidpeliculasseries.database.Entry.MovieEntry


@Dao
interface MovieDao {
    @Query("SELECT * FROM Movietetable")
    suspend fun getMovieList(): List<MovieEntry>?

    @Query("SELECT * FROM Movietetable WHERE Movietetable.id = :id")
    suspend fun getMovie(id: String): MovieEntry?

    @Insert(onConflict = IGNORE)
    suspend fun insert(movie: MovieEntry)

    @Insert(onConflict = IGNORE)
    suspend fun insert(list: List<MovieEntry>)

    @Insert(onConflict = REPLACE)
    suspend fun update(movie: MovieEntry)

    @Delete
    suspend fun deleteMovie(movie: MovieEntry)

    @Query("DELETE FROM Movietetable WHERE id = :id")
    suspend fun deleteMovie(id: String)

    @Query("DELETE FROM Movietetable")
    suspend fun deleteAll()

    @Query("SELECT * FROM Movietetable LIMIT :pageSize OFFSET :pageIndex")
    suspend fun getMoviePage(pageSize: Int, pageIndex: Int): List<MovieEntry>?
}