package giovanni.app.tools;

public class BuildConfig {
    public static final String BASE_URL = "https://api.themoviedb.org/";
    public static final String LARGE_IMAGE_URL = "https://image.tmdb.org/t/p/w500";
    public static final String ORIGINAL_IMAGE_URL = "https://image.tmdb.org/t/p/original";
    public static final String SMALL_IMAGE_URL = "https://image.tmdb.org/t/p/w200";
    public static final String TMBD_API_KEY = "/3/movie/popular?api_key=944babc69e472b7a765206267feeb43a";
    public static final String TMBD_NOW_PLAYING = "/3/movie/now_playing?api_key=944babc69e472b7a765206267feeb43a";
}
